<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Video</title>
    <link rel="stylesheet" href="pl_site_files/font-awesome.css">
    <link rel="stylesheet" href="pl_site_files/bootstrap.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    <script src="js/hls.js"></script>
    <style>
    body{
        display: flex;
        max-height: 100vh;
        height:100vh;
        width: 100%;
        margin: 0;
        padding: 0;
        position: relative;
        align-items: center;
        justify-content: center;
        background-color:black;
    }
    video{
        top:0;
        left:0;
        width:100%;
        height:100%;
    }
                              
    .volume {
        position: absolute;
        width: 130px;
        height: 130px;
        top: calc(50% - 65px);
        left: calc(50% - 65px);
        z-index: 1000;
        background: rgba(0, 0, 0, .5);
        border-radius: 5px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        cursor: pointer;
        animation: pulse 1s linear infinite;
    }

    .volume p {
        color: #fff;
        font-weight: bold;
        font-size: 18px;
    }

    .volume i {
        color: #fff;
    }

    @keyframes pulse {
        0% {
            transform: scale(1, 1);
        }

        50% {
            transform: scale(1.1, 1.1);
        }

        100% {
            transform: scale(1, 1);
        }
    }
    </style>
</head>
<body>
    <div class="volume" onclick="document.querySelector('video').muted = false; this.style.display = 'none'">
        <i class="fas fa-5x fa-volume-up unmute-btn"></i>
        <p>Volume On</p>
    </div>
    <video id="video" oncontextmenu="return false;" autoplay muted controls=true></video>
    <script>
    var video = document.getElementById('video');
    var videoSrc = 'video/video.m3u8';
    if (Hls.isSupported()) {
        var hls = new Hls();
        hls.loadSource(videoSrc);
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, function() {
        video.play();
        });
    }
    else if (video.canPlayType('application/vnd.apple.mpegurl')) {
        video.src = videoSrc;
        video.addEventListener('loadedmetadata', function() {
        video.play();
        });
    }
    </script>
</body>
</html>