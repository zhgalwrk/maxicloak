<?php if(isset($_GET['v'])):?>
<?php
	$control = 'true';
	if(isset($_GET['c']))
		$control = $_GET['c'];
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Video</title>
    <link rel="stylesheet" href="css/vlite.css">
	<style>
	body{
		margin:0;
		padding:0;
	}
	iframe{
		pointer-events:none;
	}
	</style>
</head>
<body>
		<div class="yt_video">
			<video id="player-yt-1" class="vlite-js" data-youtube-id="<?=$_GET['v']?>" data-options='{"autoplay": true, "controls": <?=$control?>, "playPause": true, "time": true, "timeline": true, "volume": true, "fullscreen": true, "poster": "https://img.youtube.com/vi/<?=$_GET['v']?>/0.jpg", "nativeControlsForTouch": true}'></video>
		</div>
		
<script type="text/javascript" src="js/vlite-html5+youtube.min.js"></script>
<script>		
    var playerYT1 = new vLite({
        selector: '#player-yt-1'
    });
</script>
</body>
</html>
<?php endif;?>

