<!DOCTYPE html>
<html lang="pl">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Poświęć na to tylko paru minut</title>
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>

	<link rel='stylesheet' id='wp-block-library-css' href='/wp-includes/css/dist/block-library/style.min.css'
		type='text/css' media='all' />

	<style id='wp-block-library-inline-css' type='text/css'>
		.has-text-align-justify {
			text-align: justify;
		}
	</style>
	<link rel='stylesheet' id='uaf_client_css-css' href='/wp-content/uploads/useanyfont/uaf.css' type='text/css'
		media='all' />
	<link rel='stylesheet' id='parent-style-css'
		href='/wp-content/cache/autoptimize/css/autoptimize_single_4a2e663bd115ebb6a3c4dabf9ef508d0.css' type='text/css'
		media='all' />
	<link rel='stylesheet' id='divi-style-css'
		href='/wp-content/cache/autoptimize/css/autoptimize_single_f2b62d2c01f925e00d15fa9fa3b47892.css' type='text/css'
		media='all' />
	<link rel='stylesheet' id='et-builder-googlefonts-cached-css'
		href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%7CCabin%3Aregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%7CAssistant%3A200%2C300%2Cregular%2C600%2C700%2C800%7CPT+Sans%3Aregular%2Citalic%2C700%2C700italic&#038;ver=5.4#038;subset=latin,latin-ext'
		type='text/css' media='all' />
	<link rel='stylesheet' id='addthis_all_pages-css'
		href='/wp-content/plugins/addthis/frontend/build/addthis_wordpress_public.min.css' type='text/css'
		media='all' />
	<link rel='stylesheet' id='dashicons-css' href='/wp-includes/css/dashicons.min.css' type='text/css' media='all' />
	<link rel='stylesheet' id='jetpack_css-css'
		href='/wp-content/cache/autoptimize/css/autoptimize_single_a84621c521bba913f3a756b031072d4c.css' type='text/css'
		media='all' />
	<script type='text/javascript'>
		/* <![CDATA[ */
		var monsterinsights_frontend = {
			"js_events_tracking": "true",
			"download_extensions": "doc,pdf,ppt,zip,xls,docx,pptx,xlsx",
			"inbound_paths": "[]",
			"home_url": "https:\/\/thebdschool.com",
			"hash_tracking": "false"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' async='async'
		src='/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js'></script>
	<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
	<script type='text/javascript' async='async' src='/wp-includes/js/jquery/jquery-migrate.min.js'></script>
	<link rel='https://api.w.org/' href='/wp-json/' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" />
	<meta name="generator" content="WordPress 5.4" />
	<link rel='shortlink' href='/?p=4575' />
	<link rel="alternate" type="application/json+oembed"
		href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthebdschool.com%2Fbusiness-development-course%2F" />
	<link rel="alternate" type="text/xml+oembed"
		href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthebdschool.com%2Fbusiness-development-course%2F&#038;format=xml" />

	<style type='text/css'>
		img#wpstats {
			display: none
		}
	</style>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<style type="text/css">
		/* If html does not have either class, do not show lazy loaded images. */
		html:not(.jetpack-lazy-images-js-enabled):not(.js) .jetpack-lazy-image {
			display: none;
		}
	</style>
	<script>
		document.documentElement.classList.add(
			'jetpack-lazy-images-js-enabled'
		);
	</script>
	<script>
		document.addEventListener('DOMContentLoaded', function (event) {

			if (window.location.hash) {
				// Start at top of page
				window.scrollTo(0, 0);

				// Prevent default scroll to anchor by hiding the target element
				var db_hash_elem = document.getElementById(window.location.hash.substring(1));
				window.db_location_hash_style = db_hash_elem.style.display;
				db_hash_elem.style.display = 'none';

				// After a short delay, display the element and scroll to it
				jQuery(function ($) {
					setTimeout(function () {
						$(window.location.hash).css('display', window.db_location_hash_style);
						et_pb_smooth_scroll($(window.location.hash), false, 800);
					}, 700);
				});
			}
		});
	</script>
	<link rel="icon"
		href="https://i2.wp.com/thebdschool.com/wp-content/uploads/2018/07/cropped-symbol-r-gradient-rbga.png?fit=32%2C32&#038;ssl=1"
		sizes="32x32" />
	<link rel="icon"
		href="https://i2.wp.com/thebdschool.com/wp-content/uploads/2018/07/cropped-symbol-r-gradient-rbga.png?fit=192%2C192&#038;ssl=1"
		sizes="192x192" />
	<link rel="apple-touch-icon"
		href="https://i2.wp.com/thebdschool.com/wp-content/uploads/2018/07/cropped-symbol-r-gradient-rbga.png?fit=180%2C180&#038;ssl=1" />
	<meta name="msapplication-TileImage"
		content="https://i2.wp.com/thebdschool.com/wp-content/uploads/2018/07/cropped-symbol-r-gradient-rbga.png?fit=270%2C270&#038;ssl=1" />
	<link rel="stylesheet" id="et-core-unified-cached-inline-styles"
		href="/wp-content/cache/et/4575/et-core-unified-15867662819926.min.css"
		onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
	<script id="mcjs">
		! function (c, h, i, m, p) {
			m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
		}(document, "script",
			"https://chimpstatic.com/mcjs-connected/js/users/928ddd3cbce8e04a0ba701801/6206145edb966fe75ef5e28bc.js");
	</script>
</head>


<body class="page-template page-template-page-template-blank page-template-page-template-blank-php page page-id-4575 et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
<?=$pixel_img_pageview?>
	<div id="page-container">

		<div id="main-content">



			<article id="post-4575" class="post-4575 page type-page status-publish hentry">


				<div class="entry-content">
					<!-- <div class="at-above-post-page addthis_tool" data-url="/business-development-course/"></div> -->
					<div id="et-boc" class="et-boc">

						<div class="et_builder_inner_content et_pb_gutters3">
							<div
								class="et_pb_section et_pb_section_0 above-the-fold et_hover_enabled et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_0 et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div class="et_pb_module et_pb_image et_pb_image_0 et_always_center_on_mobile">


											<a href="#"><span class="et_pb_image_wrap "><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/07/logo-horizontal-gradient-white-rbga.png?w=1080&#038;ssl=1"
														alt data-recalc-dims="1"
														data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/07/logo-horizontal-gradient-white-rbga.png?w=1080&amp;is-pending-load=1#038;ssl=1"
														srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
														class=" jetpack-lazy-image"><noscript><img
															src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/07/logo-horizontal-gradient-white-rbga.png?w=1080&#038;ssl=1"
															alt="" data-recalc-dims="1" /></noscript></span></a>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_1 et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h1>Rozpocznij swoją karierę biznesową</h1>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_1 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">Dołącz do naszego darmowego 5-tygodniowego internetowego kursu rozwoju biznesu i znajdź swoją wymarzoną pracę!</span></p>
												<p><span style="font-weight: 400;">Następna klasa od 6 czerwca 2021 r.</span></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_ et_pb_module ">
											<a class="et_pb_button et_pb_button_0 button-mobile-centre et_hover_enabled et_pb_bg_layout_dark"
												href="#course-check-out">Dowiedz się więcej</a>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough et_pb_column_empty">



									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_1 et_section_regular">




								<div class="et_pb_row et_pb_row_2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_3    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>COVID-19 Update: aby pomóc Ci być na bieżąco, zrobiliśmy wszystkie nasze kursy za darmo do lipca 2020 roku. Inicjatywa ta jest zarezerwowana dla studentów mieszkających w najbardziej dotkniętych obszarach. W chwili obecnej jest ona dostępna dla studentów z Włoch, Chin i Iranu. Ściśle monitorujemy sytuację i nie wykluczamy podejmowania dodatkowych działań w miarę upływu czasu. Trzymajcie się!</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_3 et_pb_equal_columns et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_2_5 et_pb_column_4    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_3 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>JEDNE FAKTY</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_4 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Twoja przyszłość zaczyna się tutaj.</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_5 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Jesteśmy z misją tworzenia nowej generacji programistów biznesowych. Wierzymy, że każdy jest wyjątkowy i uważamy, że rozwój biznesu jest właściwą drogą dla ambitnych osób, które chcą odkryć swój prawdziwy potencjał.</p>
												<p>Jesteśmy tu po to, by nauczyć Cię wszystkiego, co wiemy, by uczynić Cię profesjonalistą w dziedzinie rozwoju biznesu!</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_3_5 et_pb_column_5    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_1 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-recruitment-illustration-small.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-recruitment-illustration-small.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-recruitment-illustration-small.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div
								class="et_pb_with_border et_pb_section et_pb_section_4 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_4 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_6    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>NASZE STUDENCI</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>To jest wpływ, do którego dążymy.</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_slider et_pb_slider_0 et_hover_enabled et_pb_slider_fullwidth_off et_pb_slider_no_pagination">
											<div class="et_pb_slides">
												<div class="et_pb_slide et_pb_slide_0 et_pb_bg_layout_light et_pb_media_alignment_center et-pb-active-slide"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">"Kurs jest realizowany indywidualnie i dostosowany do własnych potrzeb, zarówno osobistych jak i biznesowych. Udział w kursie BD naprawdę pomógł mi wytyczyć jasną ścieżkę dla przyszłości LUTTO".</h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/09/sdfgjfh.png?resize=66%2C67&#038;ssl=1"
																			width="66" height="67"
																			alt="the bd school quirine wissink"
																			class="wp-image-6958 alignnone size-medium jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/09/sdfgjfh.png?resize=66%2C67&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/09/sdfgjfh.png?resize=66%2C67&#038;ssl=1"
																				width="66" height="67"
																				alt="the bd school quirine wissink"
																				class="wp-image-6958 alignnone size-medium"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span color="#161720"
																			style="color: #161720;">Quirine Wissink</span></p>
																	<p><span
																			style="color: #4d58de; font-size: small; letter-spacing: 1px;">ZAŁOŻYCIEL | LUTTO</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->
												<div class="et_pb_slide et_pb_slide_1 et_pb_bg_layout_light et_pb_media_alignment_center"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">"Kurs był naprawdę praktyczny i dał mi użyteczne narzędzia i format do wdrożenia z klientami i dodania wartości do mojej oferty".
																</h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2020/01/59784141_10162034229730554_5746084647641546752_n.png?resize=66%2C69&#038;ssl=1"
																			width="66" height="69"
																			alt="the bd school testimonial erica pew"
																			class="wp-image-7193 alignnone size-medium jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2020/01/59784141_10162034229730554_5746084647641546752_n.png?resize=66%2C69&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2020/01/59784141_10162034229730554_5746084647641546752_n.png?resize=66%2C69&#038;ssl=1"
																				width="66" height="69"
																				alt="the bd school testimonial erica pew"
																				class="wp-image-7193 alignnone size-medium"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span color="#161720"
																			style="color: #161720;">Erica Pew</span></p>
																	<p><span
																			style="color: #4d58de; font-size: small; letter-spacing: 1px;">BD KONSULTANT | KONSULTANT NA OBCASACH</span>
																	</p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->
												<div class="et_pb_slide et_pb_slide_2 et_pb_bg_layout_light et_pb_media_alignment_center"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">"BD School naprawdę zmieniła mój sposób pracy i pomogła mi uzyskać wszystkie spostrzeżenia i strategie, których potrzebowałem, aby rozwijać się i odnosić sukcesy w swojej karierze".</h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lluis-millet-escriva.png?resize=50%2C50&#038;ssl=1"
																			alt="thumb_01_90_90" width="50" height="50"
																			class="alignnone size-full wp-image-5984 jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lluis-millet-escriva.png?resize=50%2C50&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lluis-millet-escriva.png?resize=50%2C50&#038;ssl=1"
																				alt="thumb_01_90_90" width="50"
																				height="50"
																				class="alignnone size-full wp-image-5984"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span style="color: #161720;">Lluis Millet Escrivà</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																		<span
																			style="color: #4d58de; font-size: small;">DEWELOPER BIZNESOWY | ALTISERVICE ENGIE</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->
												<div class="et_pb_slide et_pb_slide_3 et_pb_bg_layout_light et_pb_media_alignment_center"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">"BD School pozwoliła nam zaoszczędzić ogromną ilość czasu podczas szkolenia naszych nowych pracowników. Pomogła nam również poprawić naszą ogólną strategię i szybciej osiągnąć nasze cele".</h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-juan-pablo-angarita.png?resize=55%2C55&#038;ssl=1"
																			alt="thumb_01_90_90" width="55" height="55"
																			class="alignnone size-full wp-image-5984 jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-juan-pablo-angarita.png?resize=55%2C55&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-juan-pablo-angarita.png?resize=55%2C55&#038;ssl=1"
																				alt="thumb_01_90_90" width="55"
																				height="55"
																				class="alignnone size-full wp-image-5984"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span style="color: #161720;">Juan-Pablo
																			Angarita</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																		<span
																			style="color: #4d58de; font-size: small;">SPRZEDAŻ I MARKETING VP | WIREMIND SAS</span>
																	</p>
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->
												<div class="et_pb_slide et_pb_slide_4 et_pb_bg_layout_light et_pb_media_alignment_center"
													data-dots_color="#4D58DE" data-arrows_color="#4D58DE">


													<div class="et_pb_container clearfix">
														<div class="et_pb_slider_container_inner">

															<div class="et_pb_slide_description">
																<h2 class="et_pb_slide_title">"TBDS pomógł mi ukształtować lepszy sposób myślenia o moim biznesie. Pomógł mi zrozumieć, jak ważna jest znajomość moich klientów i zaoferowanie właściwego rozwiązania". </h2>
																<div class="et_pb_slide_content">
																	<p><img src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-rowan-de-geus.png?resize=59%2C59&#038;ssl=1"
																			alt="thumb_01_90_90" width="59" height="59"
																			class="alignnone size-full wp-image-5984 jetpack-lazy-image"
																			data-recalc-dims="1"
																			data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-rowan-de-geus.png?resize=59%2C59&amp;is-pending-load=1#038;ssl=1"
																			srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"><noscript><img
																				src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-rowan-de-geus.png?resize=59%2C59&#038;ssl=1"
																				alt="thumb_01_90_90" width="59"
																				height="59"
																				class="alignnone size-full wp-image-5984"
																				data-recalc-dims="1" /></noscript></p>
																	<p><span style="color: #161720;">Rowan de Geus</span></p>
																	<p
																		style="color: #99c3eb; font-size: 16px; margin-top: -20px; letter-spacing: 1px; font-weight: 400;">
																		<span
																			style="color: #4d58de; font-size: small;">ZAŁOŻYCIEL | TRIKTRAKA</span></p>
																</div>

															</div> <!-- .et_pb_slide_description -->
														</div>
													</div> <!-- .et_pb_container -->

												</div> <!-- .et_pb_slide -->

											</div> <!-- .et_pb_slides -->

										</div> <!-- .et_pb_slider -->

									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_5 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_5 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_7    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>NASZA PROMISJA</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Twój sukces zaczyna się od edukacji</h2>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_6 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_1_4 et_pb_column_8    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_2 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-peer-to-peer.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-peer-to-peer.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-peer-to-peer.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Praktyczne</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_11 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Nauczysz się w praktyczny sposób wszystkiego, czego potrzebujesz, aby rozpocząć karierę w rozwoju biznesu</span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_4 et_pb_column_9    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_3 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-multidisciplinary.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-multidisciplinary.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-multidisciplinary.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_12 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Peer-2-peer</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_13 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Twoi nauczyciele są profesjonalistami w dziedzinie rozwoju biznesu pracującymi na co dzień w terenie.</span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_4 et_pb_column_10    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_4 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_14 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Online</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_15 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Postępuj zgodnie z kursem online, gdziekolwiek jesteś, planując czas, który pasuje do Twojego harmonogramu</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_4 et_pb_column_11    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_5 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-expertise.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-expertise.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-expertise.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_16 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Sieć</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_17 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Uzyskaj dostęp do naszej międzynarodowej sieci i znajdź pracę w najgorętszych europejskich startupach </span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_6 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_7">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_12    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_18 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2 style="text-align: center;">Chcesz więcej informacji? Skontaktuj się z nami już dziś!</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_center et_pb_module ">
											<a class="et_pb_button et_pb_button_1 button-mobile-centre et_hover_enabled et_pb_bg_layout_dark"
												href="#course-check-out">Zacznij uczyć się już teraz</a>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div
								class="et_pb_section et_pb_section_7 pa-hide-background-image-mobile et_section_regular">




								<div class="et_pb_row et_pb_row_8 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_13    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_19 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>OVERVIEW</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_20 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Czego się nauczysz</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_21 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h3 style="text-align: center;">Następna klasa od 29 lutego 2021 r.
												</h3>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_9 custom_row et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_14  second-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_22 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>TYGODZIEŃ 1</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_23 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Podstawy</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_24 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>W tej pierwszej klasie nauczysz się podstaw rozwoju biznesu. Zaczynając od jego definicji, aż do rozwoju kariery. Wyjdziesz z tej sesji z jasnym zarysem tego, jaka może być Twoja przyszłość.</p>
												<p>Po tych zajęciach będziesz:</p>
												<p><strong>Własny sposób myślenia o rozwoju biznesu</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div class="et_pb_module et_pb_image et_pb_image_6 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><noscript><img src alt></noscript><img
													class="lazyload" src alt><noscript><img src=""
														alt="" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_15  first-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_7 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-1.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-1.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-1.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_10 et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_16    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_8 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-2.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-2.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-2.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_17    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_25 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>TYGODZIEŃ 2</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_26 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Strategie</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_27 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">W tej klasie przedstawimy Państwu 4 główne strategie rozwoju biznesu. Poznasz podstawy sprzedaży, marketingu, produktów i partnerstwa strategicznego. Przeprowadzimy praktyczne ćwiczenia i wykorzystamy je w rzeczywistych przypadkach biznesowych.</span></p>
												<p><span style="font-weight: 400;">Po tych zajęciach będziesz:</span></p>
												<p><strong>Własne podstawy strategicznego rozwoju biznesu</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_11 custom_row et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_18  second-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_28 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>TYGODZIEŃ 3</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_29 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Wykonanie</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_30 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">W tej klasie wprowadzimy Cię w główne taktyki rozwoju biznesu, aby zidentyfikować Twoją grupę docelową, wygenerować leady, podejść do potencjalnych klientów i partnerów, zbudować wartościowe powiązania i wygenerować wartość. Będziemy korzystać z roleplayów i przypadków biznesowych.</span></p>
												<p><span style="font-weight: 400;">Po tych zajęciach będziesz:</span></p>
												<p><strong>Opanuj główną taktykę rozwoju biznesu</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div class="et_pb_module et_pb_image et_pb_image_9 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><noscript><img src alt></noscript><img
													class="lazyload" src alt><noscript><img src=""
														alt="" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_19  first-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_10 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-3.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-3.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-3.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_12 et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_20    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_11 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-4.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-4.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-4.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_21    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_31 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>TYGODZIEŃ 4</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_32 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Umiejętności miękkie</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_33 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">W tej klasie wprowadzimy Cię w główne umiejętności miękkie niezbędne do rozwoju biznesu. Będziemy ćwiczyć badania, komunikację, negocjacje i jak być bardziej produktywnym. Będziemy ćwiczyć z grami fabularnymi, ćwiczeniami i case'ami biznesowymi.</span></p>
												<p><span style="font-weight: 400;">Po tych zajęciach będziesz:</span></p>
												<p><strong>Opanowanie głównych umiejętności w zakresie rozwoju biznesu</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_13 custom_row et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_22  second-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_34 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>TYGODZIEŃ 5</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_35 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Kariera </p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_36 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span style="font-weight: 400;">W tej klasie skupimy się na rozwoju kariery. Nauczymy Cię, jak znaleźć pracę w erze cyfrowej, jak uspokoić swoje CV, jak przeprowadzić udaną rozmowę kwalifikacyjną i jak zbudować wymarzoną karierę..</span></p>
												<p><span style="font-weight: 400;">Po tych zajęciach będziesz:</span></p>
												<p><strong>Znajdź swoją wymarzoną pracę</strong></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div class="et_pb_module et_pb_image et_pb_image_12 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><noscript><img src alt></noscript><img
													class="lazyload" src alt><noscript><img src=""
														alt="" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_23  first-on-mobile  et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_13 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-5.jpg?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-5.jpg?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/03/the-bd-school-week-5.jpg?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_8 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_14 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_24    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_37 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>CO JEST WLICZONE</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_38 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Niektóre narzędzia, które pomogą Ci osiągnąć sukces</h2>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_15 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_25    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_14 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-chat.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-chat.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i2.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-chat.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_39 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Wirtualne klasy</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_40 et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Będziesz spotykać się z trenerem online co tydzień, przez 2 godziny przez cały czas trwania kursu.</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_26    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_15 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-qa.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-qa.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-qa.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_41 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Sesje pytań i odpowiedzi</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_42 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Twój trener będzie dostępny przez 1 godzinę w tygodniu, aby odpowiedzieć na każde pytanie lub wyjaśnienie.</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_27    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_16 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-homework.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-homework.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-homework.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_43 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Praca domowa Ćwiczenia</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_44 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Każdego tygodnia będziesz otrzymywać pracę domową i ćwiczenia, aby ćwiczyć to, czego się właśnie nauczyłeś.</span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_16 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_28    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_17 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-certificate.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-certificate.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-certificate.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_45 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Certificate</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_46 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Po ukończeniu kursu rozwoju biznesu otrzymają Państwo oficjalny certyfikat</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_29    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_18 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-globe.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_47 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Dostęp do naszej społeczności</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_48 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Otrzymasz dostęp do naszej internetowej społeczności, gdzie będziesz mógł spotkać się z innymi studentami.</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_1_3 et_pb_column_30    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_19 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lock-open.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lock-open.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i0.wp.com/thebdschool.com/wp-content/uploads/2019/06/the-bd-school-lock-open.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>
										<div
											class="et_pb_module et_pb_text et_pb_text_49 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h4>Sieć</h4>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_50 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p style="text-align: center;"><span>Będziesz połączony z rekruterami i doradcami zawodowymi, aby zbudować swoją wymarzoną przyszłość.</span>
												</p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_9 et_section_regular">




								<div class="et_pb_row et_pb_row_17 et_pb_gutters2">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_31    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_51 et_pb_bg_layout_light  et_pb_text_align_center">


											<div class="et_pb_text_inner">
												<p>POZNAĆ SWOJEGO TRENERA</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_52 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Cześć, jestem twoim trenerem!</h2>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_18 et_pb_equal_columns et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_2_5 et_pb_column_32    et_pb_css_mix_blend_mode_passthrough">


										<div class="et_pb_module et_pb_image et_pb_image_20 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><img
													src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/01/the-bd-school-lucia-min.png?w=1080&#038;ssl=1"
													alt data-recalc-dims="1"
													data-lazy-src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/01/the-bd-school-lucia-min.png?w=1080&amp;is-pending-load=1#038;ssl=1"
													srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
													class=" jetpack-lazy-image"><noscript><img
														src="https://i1.wp.com/thebdschool.com/wp-content/uploads/2019/01/the-bd-school-lucia-min.png?w=1080&#038;ssl=1"
														alt="" data-recalc-dims="1" /></noscript></span>
										</div>

										<div class="et_pb_module et_pb_image et_pb_image_21 et_always_center_on_mobile">


											<span class="et_pb_image_wrap "><noscript><img src alt></noscript><img
													class="lazyload" src alt><noscript><img src=""
														alt="" /></noscript></span>
										</div>
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_3_5 et_pb_column_33    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_53 center-align et_hover_enabled et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>Lucia Piseddu</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_54 center-align et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span>Ostatnie 10 lat spędziłem pracując nad rozwojem biznesu. W najbliższych tygodniach podzielę się z wami moimi doświadczeniami. Razem wyruszymy w niesamowitą podróż, która da Ci solidne podstawy do budowania przyszłości, na którą zasługujesz!</span></p>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div id="course-check-out"
								class="et_pb_section et_pb_section_10 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_19">
									<div
										class="et_pb_column et_pb_column_2_5 et_pb_column_34    et_pb_css_mix_blend_mode_passthrough">


										<div
											class="et_pb_module et_pb_text et_pb_text_55 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p>ILE TO KOSZTUJE</p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_56 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>BEZPŁATNY</h2>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_57 center-align  et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<p><span>5-tygodniowy intensywny kurs budujący karierę Twoich marzeń!</span></p>
											</div>
										</div> <!-- .et_pb_text -->
										<div
											class="et_pb_module et_pb_text et_pb_text_58 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<ul>
													<li>Płacić tylko w przypadku zatrudnienia*</li>
													<li>Naucz się nowej pracy od podstaw</li>
													<li>Praca domowa Ćwiczenia</li>
													<li>Dostęp do społeczności</li>
													<li>Dostęp do sieci rekrutacyjnej</li>
													<li>Certyfikat</li>
												</ul>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->
									<div
										class="et_pb_column et_pb_column_3_5 et_pb_column_35    et_pb_css_mix_blend_mode_passthrough">



										<div id="et_pb_contact_form_0"
											class="et_pb_with_border et_pb_module et_pb_contact_form_0 et_hover_enabled et_pb_contact_form_container clearfix  et_pb_text_align_left"
											data-form_unique_num="0"
											data-redirect_url="https://calendly.com/lucia-tbds/call">


											<h1 class="et_pb_contact_main_title">Kurs rozwoju biznesu</h1>
											<div class="et-pb-contact-message"></div>

											<div class="et_pb_contact">
												<form class="et_pb_contact_form clearfix xx_form" method="post"
													action="/thanks.php">
													<p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_last"
														data-id="name" data-type="input">
														<label for="et_pb_contact_name_0"
															class="et_pb_contact_form_label">Imię </label>
														<input type="text" id="et_pb_contact_name_0" class="input"
															value="" name="et_pb_contact_name_0"
															data-required_mark="required" data-field_type="input"
															data-original_id="name" placeholder="Imię ">
													</p>
													<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_last"
														data-id="company" data-type="input">
														<label for="et_pb_contact_company_0"
															class="et_pb_contact_form_label">Nazwa firmy</label>
														<input type="text" id="et_pb_contact_company_0" class="input"
															value="" name="et_pb_contact_company_0"
															data-required_mark="required" data-field_type="input"
															data-original_id="company" placeholder="Nazwa firmy">
													</p>
													<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_last"
														data-id="email" data-type="email">
														<label for="et_pb_contact_email_0"
															class="et_pb_contact_form_label">Adres e-mail</label>
														<input type="text" id="et_pb_contact_email_0" class="input"
															value="" name="et_pb_contact_email_0"
															data-required_mark="required" data-field_type="email"
															data-original_id="email" placeholder="Adres e-mail">
													</p>
													<p class="et_pb_contact_field et_pb_contact_field_3 et_pb_contact_field_last"
														data-id="phone" data-type="input">
														<label for="et_pb_contact_phone_0"
															class="et_pb_contact_form_label">Numer telefonu</label>
														<input type="text" id="et_pb_contact_phone_0" class="input"
															value="" name="et_pb_contact_phone_0"
															data-required_mark="required" data-field_type="input"
															data-original_id="phone" placeholder="Numer telefonu"
															pattern="[A-Z|a-z|0-9]*"
															title="Only letters and numbers allowed.">
													</p>
													<input type="text" value="" name="et_pb_contactform_validate_0"
														class="et_pb_contactform_validate_field" />
													<div class="et_contact_bottom_container">

														<button type="submit" class="et_pb_contact_submit et_pb_button"
															onclick="window.location = '/thanks.php'">ZASTOSOWANIE</button>
													</div>
													
												</form>
											</div> <!-- .et_pb_contact -->
										</div> <!-- .et_pb_contact_form_container -->

									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->


							</div> <!-- .et_pb_section -->
							<div class="et_pb_section et_pb_section_11 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_20 et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_36    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_text et_pb_text_59 et_pb_bg_layout_light  et_pb_text_align_left">


											<div class="et_pb_text_inner">
												<h2>Pytania, które miałem przed rozpoczęciem kursu</h2>
											</div>
										</div> <!-- .et_pb_text -->
									</div> <!-- .et_pb_column -->


								</div> <!-- .et_pb_row -->
								<div class="et_pb_row et_pb_row_21 et_pb_gutters3">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_37    et_pb_css_mix_blend_mode_passthrough et-last-child">


										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_0 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">1. Kto może zapisać się na ten kurs?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span color="#3a3c59">Ten kurs jest przeznaczony dla każdego, kto jest i chce wyjść poza swoje granice. Ponieważ skupia się na budowaniu kariery, jest otwarty na profesjonaliści poszukujący kolejnego kroku zawodowego w biznesie rozwój. Nasi studenci są świadomi, że zawsze istnieje coś więcej do osiągnięcia i nie boją się naciskać swoich Ograniczenia. Nie musisz być w biznesie przez dekadę lub nawet studiował tematy związane z biznesem. Wszystko czego potrzebujesz to być głodny sukcesu, chętny do inwestowania na siebie i otwarty do nowych możliwości, które nadejdą w twojej drodze. </span></p>
												<p><span color="#3a3c59">Naszą misją jest pomóc Ci znaleźć Twój prawdziwy potencjał i nauczyć Cię umiejętności i wiedzy, które pomogą Ci znaleźć pracę Twoich marzeń.</span><span color="#3a3c59">
												Rozwój biznesu to niesamowita dziedzina. Jest kreatywny, ekscytujący i może naprawdę zmienić Twoje życie w sposób, którego się nie spodziewasz. Jeśli będziesz chciał</span><span color="#3a3c59">g aby się uczyć, rzucić sobie wyzwanie i wejść do niezbadanej wody, wtedy jesteśmy dla Ciebie właściwą szkołą.</span></p>
												<p><span color="#3a3c59">Nauczymy Cię wszystkiego, co wiemy o rozwoju biznesu, mając jeden cel: pozwolić Ci zbudować wspaniałą karierę!</span></p>
											</div> <!-- .et_pb_toggle_content -->
										</div> <!-- .et_pb_toggle -->
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_1 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">2. Jak mogę się zgłosić?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>Aby zostać przyjętym w trakcie kursu, będziesz miał pierwszy nabór do oceny swojej sprawy. </span></p>
												<p><span>Jeśli zdasz pierwszy seans, będziesz musiał przesłać nam swoje CV i film wyjaśniający, dlaczego nadajesz się do tego programu. </span></p>
												<p><span>Następnie wybierzemy najbardziej inspirujące historie od ludzi, którzy naprawdę chcą zmienić swoją przyszłość. Uczynimy tych studentów najlepszymi deweloperami biznesowymi na rynku i pomożemy im </span></p>
												<p>Masz to, co trzeba? Kliknij przycisk "Zgłoś się" lub wyślij e-mail na adres i wkrótce będziemy w kontakcie!</span></p>
											</div>
										</div> 
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_2 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">3. Kiedy zaczyna się kurs?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>Kolejne zajęcia rozpoczną się 11 stycznia 2020 r., a it`s w pełni online. Mamy siedzibę w Rotterdamie, w Holandii. Jeśli znajdujesz się w innej strefie czasowej, skontaktuj się z nami, a my zorganizujemy wykłady zgodnie z Twoimi potrzebami. </span></p>
											</div>
										</div>
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_3 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">4. Jaki jest sposób prowadzenia kursu?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>Kurs jest online i może być wykonany w 100% zdalnie. Kurs trwa 5 tygodni i co tydzień spotkasz swojego trenera przez 2 godziny. Dodatkowo, w każdy czwartek organizujemy 1-godzinną sesję pytań i odpowiedzi, podczas której możesz zadać wyjaśnienia lub inne pytania dotyczące tego, czego się właśnie nauczyłeś. Sesja ta jest organizowana co tydzień w naszej ekskluzywnej społeczności studenckiej. </span></p>
											</div> 
										</div>
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_4 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">5. Ile godzin tygodniowo wymaga kurs</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>Aby pomyślnie ukończyć kurs, będziesz musiał śledzić każdą lekcję co tydzień w ciągu 4 tygodni. Na stronie czas trwania każdej lekcji wynosi 2 godziny. Każda klasa zawiera pracę domową które będziecie musieli wykonać sami w dniach następujących po klasa. Szacowany czas na odrobienie pracy domowej wynosi około 2 godziny. W sumie, zainwestujesz około 20 godzin w ciągu w ciągu 5 tygodni. Pod koniec kursu weźmiesz udział w do ostatecznego testu. Jeśli pomyślnie zdasz ten test, będziesz otrzymać certyfikat ukończenia, który można wykorzystać do zwiększenia twoja wartość na rynku.</span></p>
											</div>
										</div> 
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_5 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">6. Co się stanie po zapisaniu się na kurs
											</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>Po pomyślnym zapisaniu się na kurs otrzymasz wiadomość e-mail od jednego z naszych trenerów. E-mail ten będzie zawierał link do pierwszego wykładu oraz kilka materiałów do przygotowania. Nasi trenerzy będą w stałym kontakcie przez cały czas trwania kursu, dbając o to, aby wszystko było dla Ciebie łatwe. Możesz skontaktować się z nami w każdej chwili za pomocą formularza <kontakt> lub wysyłając e-mail. </span>
												</p>
											</div>
										</div>
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_6 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">7. Ile kosztuje kurs?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>Po otrzymaniu zgłoszeń, dokładnie je przeanalizujemy i wybierzemy historie, które najbardziej nas zainspirowały. Ponieważ naszą misją jest umożliwienie osobom indywidualnym budowania udanej kariery zawodowej w biznesie, kurs jest <strong>całkowicie darmowy</strong>. Zapłacisz tylko wtedy, gdy znajdziesz pracę w kolejnych miesiącach. Koszt wynosi <strong>1.499,00</strong> €, który zapłacisz po rozpoczęciu pracy u nowego pracodawcy.  Jeśli nie znajdziesz pracy, kurs jest u nas. Wierzymy w Ciebie i chcemy być częścią Twojego sukcesu dzieląc się z Tobą wszystkim, co wiemy o rozwoju biznesu.</span></p>
											</div>
										</div> 
										<div
											class="et_pb_module et_pb_toggle et_pb_toggle_7 et_pb_toggle_item  et_pb_toggle_close">


											<h5 class="et_pb_toggle_title">8. Czy mogę zdecydować się na płatności ratalne?</h5>
											<div class="et_pb_toggle_content clearfix">
												<p><span>Tak, mamy plan ratalny! Po pomyślnym znalezieniu pracy, możesz zapłacić za kurs w ciągu 3 miesięcy (500 € miesięcznie). Po zapisaniu się na kurs podpiszesz umowę o płatności. </span></p>
											</div> 
										</div>
									</div> 


								</div>


							</div>
							<div class="et_pb_section et_pb_section_12 et_pb_with_background et_section_regular">




								<div class="et_pb_row et_pb_row_22">
									<div
										class="et_pb_column et_pb_column_4_4 et_pb_column_38    et_pb_css_mix_blend_mode_passthrough et-last-child">
										<div
											class="et_pb_module et_pb_text et_pb_text_60 et_pb_bg_layout_light  et_pb_text_align_left">
											<div class="et_pb_text_inner">
												<h2 style="text-align: center;">Gotowy do rozpoczęcia podróży edukacyjnej?
												</h2>
											</div>
										</div>
										<div
											class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_center et_pb_module ">
											<a class="et_pb_button et_pb_button_2 button-mobile-centre et_hover_enabled et_pb_bg_layout_dark"
												href="#course-check-out">Zarejestruj się</a>
										</div>
									</div>
								</div>
							</div>
							<div class="et_pb_section et_pb_section_16 et_pb_with_background et_section_regular">

<style>
	.footer-policy{
		font-weight: bold;
		font-size: 22px;
		transition: .3s;
		text-decoration: underline;
	}
	.footer-policy:hover{
		color: #333;
		text-decoration: underline;

	}
	.footer-phone{
		display: flex;
		flex-direction: column-reverse;
		padding-top: 10px;

	}
</style>
								<div class="et_pb_row et_pb_row_24">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_44 et_pb_css_mix_blend_mode_passthrough">
										<a href="./Privacy.html" class="footer-policy">Polityka prywatności</a>
										
									</div>
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_44 et_pb_css_mix_blend_mode_passthrough">
										<div class="footer-phone">
										</div>
									</div>
									
								</div>
								<br>
								<br>
								<div class="et_pb_row et_pb_row_24">
									<div
										class="et_pb_column et_pb_column_1_2 et_pb_column_43 et_pb_css_mix_blend_mode_passthrough">
										<div
											class="et_pb_module et_pb_text et_pb_text_67 center-align  et_pb_bg_layout_light  et_pb_text_align_right">
											<div class="et_pb_text_inner">
												<p style="text-align: left;">©2020 Simpek. Wszelkie prawa zastrzeżone.
												</p>
											</div>
										</div>
									</div>
								

								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>


	<style>
		.lazyload,
		.lazyloading {
			opacity: 0;
		}

		.lazyloaded {
			opacity: 1;
			transition: opacity 300ms;
		}
	</style>

	<noscript>
		<style>
			.lazyload {
				display: none;
			}
		</style>
	</noscript>

	<script data-noptimize="1">
		window.lazySizesConfig = window.lazySizesConfig || {};
		window.lazySizesConfig.loadMode = 1;
	</script>
	<script async data-noptimize="1"
		src='/wp-content/plugins/autoptimize/classes/external/js/lazysizes.min.js?ao_version=2.6.2'></script>
	<script data-noptimize="1">
		function c_webp(A) {
			var n = new Image;
			n.onload = function () {
				var e = 0 < n.width && 0 < n.height;
				A(e)
			}, n.onerror = function () {
				A(!1)
			}, n.src = 'data:image/webp;base64,UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA=='
		}

		function s_webp(e) {
			window.supportsWebP = e
		}
		c_webp(s_webp);
		document.addEventListener('lazybeforeunveil', function ({
			target: c
		}) {
			supportsWebP && ['data-src', 'data-srcset'].forEach(function (a) {
				attr = c.getAttribute(a), null !== attr && c.setAttribute(a, attr.replace(/\/client\//,
					'/client/to_webp,'))
			})
		});
	</script>
	<script>
		(function () {
			// Override the addClass to prevent fixed header class from being added
			var addclass = jQuery.fn.addClass;
			jQuery.fn.addClass = function () {
				var result = addclass.apply(this, arguments);
				jQuery('#main-header').removeClass('et-fixed-header');
				return result;
			}
		})();
		jQuery(function ($) {
			$('#main-header').removeClass('et-fixed-header');
		});
	</script>
	<script data-cfasync="false" type="text/javascript">
		if (window.addthis_product === undefined) {
			window.addthis_product = "wpp";
		}
		if (window.wp_product_version === undefined) {
			window.wp_product_version = "wpp-6.2.6";
		}
		if (window.addthis_share === undefined) {
			window.addthis_share = {};
		}
		if (window.addthis_config === undefined) {
			window.addthis_config = {
				"data_track_clickback": true,
				"ui_atversion": "300"
			};
		}
		if (window.addthis_plugin_info === undefined) {
			window.addthis_plugin_info = {
				"info_status": "enabled",
				"cms_name": "WordPress",
				"plugin_name": "Share Buttons by AddThis",
				"plugin_version": "6.2.6",
				"plugin_mode": "AddThis",
				"anonymous_profile_id": "wp-caaf6d80e67ecb82cbb72516e6643c75",
				"page_info": {
					"template": "pages",
					"post_type": ""
				},
				"sharing_enabled_on_post_via_metabox": false
			};
		}
		(function () {
			var first_load_interval_id = setInterval(function () {
				if (typeof window.addthis !== 'undefined') {
					window.clearInterval(first_load_interval_id);
					if (typeof window.addthis_layers !== 'undefined' && Object.getOwnPropertyNames(window
							.addthis_layers).length > 0) {
						window.addthis.layers(window.addthis_layers);
					}
					if (Array.isArray(window.addthis_layers_tools)) {
						for (i = 0; i < window.addthis_layers_tools.length; i++) {
							window.addthis.layers(window.addthis_layers_tools[i]);
						}
					}
				}
			}, 1000)
		}());
	</script>
	<script type='text/javascript' async='async'
		src='/wp-content/plugins/jetpack/_inc/build/photon/photon.min.js?ver=20191001'></script>
	<script type='text/javascript'>
		var leadin_wordpress = {
			"userRole": "visitor",
			"pageType": "page",
			"leadinPluginVersion": "7.25.2"
		};
	</script>
	
	<script type='text/javascript'>
		var DIVI = {
			"item_count": "%d Item",
			"items_count": "%d Items"
		};
		var et_shortcodes_strings = {
			"previous": "Previous",
			"next": "Next"
		};
		var et_pb_custom = {
			"images_uri": "https:\/\/thebdschool.com\/wp-content\/themes\/Divi\/images",
			"builder_images_uri": "https:\/\/thebdschool.com\/wp-content\/themes\/Divi\/includes\/builder\/images",
			"et_frontend_nonce": "fe4ca74448",
			"subscription_failed": "Please, check the fields below to make sure you entered the correct information.",
			"et_ab_log_nonce": "ac70f978db",
			"fill_message": "Please, fill in the following fields:",
			"contact_error_message": "Please, fix the following errors:",
			"invalid": "Invalid email",
			"captcha": "Captcha",
			"prev": "Prev",
			"previous": "Previous",
			"next": "Next",
			"wrong_captcha": "You entered the wrong number in captcha.",
			"ignore_waypoints": "no",
			"is_divi_theme_used": "1",
			"widget_search_selector": ".widget_search",
			"is_ab_testing_active": "",
			"page_id": "4575",
			"unique_test_id": "",
			"ab_bounce_rate": "5",
			"is_cache_plugin_active": "no",
			"is_shortcode_tracking": "",
			"tinymce_uri": ""
		};
		var et_pb_box_shadow_elements = [];
	</script>
	<script type='text/javascript' async='async' src='/wp-content/themes/Divi/js/custom.min.js'></script>
	
	<script type='text/javascript' async='async'
		src='/wp-content/plugins/jetpack/_inc/build/lazy-images/js/lazy-images.min.js'></script>
	<script type='text/javascript' async='async'
		src='/wp-content/cache/autoptimize/js/autoptimize_single_82b34a0f20682b94458a89521a92c7ca.js'>
	</script>
	<script type='text/javascript' async='async' src='/wp-includes/js/wp-embed.min.js'></script>
	<script type='text/javascript' src='https://stats.wp.com/e-202016.js' async='async' defer='defer'></script>
	<script type='text/javascript'>
		_stq = window._stq || [];
		_stq.push(['view', {
			v: 'ext',
			j: '1:8.3',
			blog: '149801255',
			post: '4575',
			tz: '0',
			srv: 'thebdschool.com'
		}]);
		_stq.push(['clickTrackerInit', '149801255', '4575']);
	</script>
</body>

</html>

