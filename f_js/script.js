        
        function rand(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function copies() {
            var el = $("#copies");
            var left = parseInt($(el).html());

            left = left > 5 ? left - rand(1, 3) : left - rand(-2, 2);
            if (left < 2) {
                $(el).html(1);
            } else $(el).html(left);
            $("#watchers").html(rand(701, 1998));
            setTimeout('copies()', rand(6000, 9000));
        }

        function winners() {
            var feedback = $.ajax({
                type: "GET",
                url: "wgt.php?q=right",
                async: false
            }).responseText;

            var el = $(".member_earn");

            el.html(feedback);
            setTimeout('winners()', rand(6000, 9000));
        }

        $(function () {
            copies();
            winners();
        });