<!DOCTYPE html>
<html>

<head>
     <title>Kredit Online</title>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width">
     <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,500,600,700,900" rel="stylesheet">

     <meta name='robots' content='noindex,follow' />

     <style type="text/css">
          img.wp-smiley,
          img.emoji {
               display: inline !important;
               border: none !important;
               box-shadow: none !important;
               height: 1em !important;
               width: 1em !important;
               margin: 0 .07em !important;
               vertical-align: -0.1em !important;
               background: none !important;
               padding: 0 !important;
          }

          .wrapper {
               max-width: 1200px;
               width: 100%;
               margin: 0 auto;
               padding: 0 30px;
          }

          .footer {
               text-align: center;
          }
     </style>

</head>

<body class="page">
     <div class="wrapper">

          <div class="content kredit_page">
               <h2>PUBLIC OFFER</h2>


               <p style="font-size: 16px; line-height: 30px;">Agreement for the provision of services using the Service
               </p>

               <p><strong style="font-size: 18px; line-height: 20px;">1. General Provisions.</strong></p>

               <p style="font-size: 16px; line-height: 30px;">This public offer (Offer) on the services providing is a
                    public offer on the website profitmaximizert.info (hereinafter referred to as the Operator) and
                    defines the conditions for the provision of services and the provision of services to users
                    (hereinafter the Agreement). This Offer of addressed persons, has the proper right and legal
                    capacity, is the official representative of the site.</p>

               <p><strong style="font-size: 18px; line-height: 20px;">2. Subject of the Agreement</strong></p>
               <p style="font-size: 16px; line-height: 30px;">2.1. This Agreement defines the terms and conditions of
                    the operator.</p>
               <p style="font-size: 16px; line-height: 30px;">2.2. Acceptance of the agreement is a prerequisite for
                    consent.</p>
               <p style="font-size: 16px; line-height: 30px;">2.2. Acceptance (acceptance) of the terms of this
                    Agreement is the putting of a mark of acceptance of the terms of this Agreement in the registration
                    field at the time of registration of the User.</p>

               <p><strong style="font-size: 18px; line-height: 20px;">3. The procedure and conditions for the provision
                         of services.</strong></p>
               <p style="font-size: 16px; line-height: 30px;">3.1. Registration on the site.</p>
               <p style="font-size: 16px; line-height: 30px;">3.1.1. To gain access to the services, the User is obliged
                    to join the terms of this Agreement when registering the User on the site. The marking by the User
                    of acceptance of the terms of this Agreement at the time of registration of the User on the site is
                    an acceptance of this Agreement.</p>
               <p style="font-size: 16px; line-height: 30px;">3.1.2. User registration on the site is carried out by
                    filling in the appropriate registration form. When registering on the site, the User enters the last
                    name, first name, middle name, phone number, email address and some other data as desired.</p>

               <p><strong style="font-size: 18px; line-height: 20px;">4. Rights and obligations of the Parties.</strong>
               </p>
               <p style="font-size: 16px; line-height: 30px;">4.1. Rights and obligations of the Operator:</p>
               <p style="font-size: 16px; line-height: 30px;">4.1.1 The Operator shall provide the User with the
                    opportunity of round-the-clock access to the site.</p>
               <p style="font-size: 16px; line-height: 30px;">4.1.2. The operator undertakes to inform the Users about
                    changes (additions) to the terms of the Agreement by publishing a new edition on the site.</p>
               <p style="font-size: 16px; line-height: 30px;">4.1.3. The operator has the right to suspend the operation
                    of the site and / or the Service, as well as hardware and software, ensuring the interaction of the
                    parties within the framework of this Agreement, if significant malfunctions, errors and failures are
                    detected, as well as in order to carry out preventive work and prevent unauthorized access.</p>
               <p style="font-size: 16px; line-height: 30px;">4.2. Rights and obligations of the User:</p>
               <p style="font-size: 16px; line-height: 30px;">4.2.1. User agrees to abide by the rules of this
                    Agreement.</p>
               <p style="font-size: 16px; line-height: 30px;">4.2.2. The user agrees to provide reliable information
                    during the registration process on the site.</p>
               <p style="font-size: 16px; line-height: 30px;">4.2.3. The user agrees not to reproduce, repeat or copy,
                    sell or resell, or use information from the site for any commercial purposes.</p>
               <p style="font-size: 16px; line-height: 30px;">4.2.4. The user has the right to withdraw the consent to
                    the processing of personal data.</p>

               <p><strong style="font-size: 18px; line-height: 20px;">5. Personal Information</strong></p>
               <p style="font-size: 16px; line-height: 30px;">5.1. By registering on the site, the User agrees to the
                    processing of personal data in accordance with Federal Law of July 27, 2006 N 152-FZ "On Personal
                    Data". Personal information includes any information directly or indirectly related to the User
                    received both from the User and from third parties, including, but not limited to: last name, first
                    name, middle name (if any), date and place of birth, passport and other installation data,
                    citizenship, registration address, address of actual residence, taxpayer identification number,
                    insurance certificate number, contact phones, email addresses, as well as other information reported
                    by the User to any statutory way.</p>
               <p style="font-size: 16px; line-height: 30px;">5.2. The purpose of processing the User’s personal data is
                    to provide the User with the services presented on the Operator’s website and on the websites of the
                    Operator’s partners.</p>
               <p style="font-size: 16px; line-height: 30px;">5.3. The user agrees to receive messages about promotions
                    from the Operator and partners of the Operator in any way using the phone numbers, email and browser
                    and device identification information left during registration.</p>
               <p style="font-size: 16px; line-height: 30px;">5.4 In the event that the User provides the personal data
                    of third parties, he confirms and guarantees that the consent of these parties for transferring
                    their personal data and the processing of this personal data and/or by third parties on his behalf
                    has been obtained.</p>
               <p style="font-size: 16px; line-height: 30px;">5.5. Processing of the User’s personal data is carried out
                    within the period from the moment of registration of the User on the website until the User
                    withdraws the Consent to the processing of personal data.</p>

               <p><strong style="font-size: 18px; line-height: 20px;">6. Responsibility of the parties.</strong></p>
               <p style="font-size: 16px; line-height: 30px;">6.1. In the event of failure to fulfill or improper
                    performance of the terms of this Agreement, the Parties shall be liable under this Agreement and the
                    law.</p>
               <br>
               <p style="font-size: 16px; line-height: 30px;">For all questions, you can contact us via
                    info@<?=$_SERVER['HTTP_HOST']?></p>
               <br>
               <br>
               <br>
               <br>

               <div class="footer">
                    <div class="content footer-content">
                         <p class="copyrights">Copyright © <?=date("Y")?>
                         </p>

                    </div>
               </div>
          </div>
     </div>
</body>

</html>