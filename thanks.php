<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<title>Dziękujemy!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/css/reset.css">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/thanks.css">
	<?=$metrika?>
</head>
<body class="thanks_page">

<?=$pixel_img?>

<p> Gratulacje za udaną rejestrację w systemie. Dla naliczenia bonusu i zapewnienia dostępu do systemu, musimy upewnić się, że jesteś prawdziwą osobą, a nie robotem! W ciągu 24 godzin czekaj na telefon, aby potwierdzić swoje konto i dokończyć rejestrację - nie przegap tego telefonu, w przeciwnym razie twoje miejsce może dostać inny uczestnik systemu!</p>
<p> Sprawdź poprawność wprowadzonych danych, jeśli popełniłeś błąd, możesz wrócić i podać poprawne dane!</p>
<a href="/">Powrót</a>
</body>
</html>