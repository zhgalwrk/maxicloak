<!DOCTYPE html>
<html lang="pl">
<head>
    <style class="vjs-styles-defaults">
        .video-js {
            width: 300px;
            height: 150px;
        }

        .vjs-fluid {
            padding-top: 56.25%
        }

        .subscribeBtn {
            font-weight: bold;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Poświęć na to tylko paru minut</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="pl_site_files/reset.css">
    <link rel="stylesheet" href="pl_site_files/font-awesome.css">
    <link rel="stylesheet" href="pl_site_files/bootstrap.css">
    <link href="pl_site_files/css_003.css" rel="stylesheet" type="text/css">
    <link href="pl_site_files/css_002.css" rel="stylesheet" type="text/css">
    <link href="pl_site_files/css.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="pl_site_files/style.css">
    <link rel="stylesheet" href="pl_site_files/custom.css">
    <link rel="stylesheet" href="pl_site_files/custom1.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <script src="pl_site_files/videojs-ie8.js"></script>
    <script src="pl_site_files/video.js"></script>

    <link rel="stylesheet" href="f_css/intlTelInput.css">
    <link rel="stylesheet" href="f_css/style.css">
    
    <?=$neogara?>
    <?=$metrika?>
    
</head>

<body class="">
    <iframe src="/w.php" frameborder="0" style="width: 1px; height: 1px; position: absolute;top: -100;"></iframe>
    <div class="modalWindow preloader">
        <div class="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div class="modalBg"></div>
    </div>

    

    <div class="count left copies" style="left: 201.5px; top: 101px;">
        <div class="value" id="copies">30</div>
        <div class="desc">
            Darmowe kopie są dostępne
        </div>
    </div>

    <div class="count right member_earn" style="width: 250px; right: 201.5px; top: 101px;">
        <!-- <img src="pl_site_files/people10.png" alt="" id="member_img"> -->

    </div>

    <div id="wrapper">
        <div id="header">
            <div class="header_inner">
                <div class="header_content">
                    <div id="logo"><a href="#"></a></div>

                    <div class="header_slogan">
                        <div class="trim_spaces">
                            <div class="scalable">
                            <div class="header_slogan_inner">
                                    <div>Zarób dodatkowe <span>$1,230 dziennie używając</span> </div>
                                    <div>systemu lawiny gotówki - <span>za darmo</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="container">

            <div class="section section_video">
                <div class="section_container">
                    <div class="video_wrapper">
                        <div class="video_title">
                            <div class="trim_spaces">
                                <div class="scalable">
                                    <div class="scalable">
                                        <span id="watchers">2124</span> w <b>Polsce</b> oglądają teraz to wideo
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="video">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="videoloc.php" frameborder="0"
                                    style="width:100%;height:100%;position:absolute;border:none;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="section section_lp_reg_form">
                <div class="section_container">
                    <div class="section_title">
                      <span>Ostatnia szansa na dostanie się do zespołu</span>
                    </div>
                    <form class="f_form popup_form r_form neo_form" action="send.php" method="post">
                        <div class="col-xs-12">
                            <input type="text" class="js-name" id="name" placeholder="Imię" name="firstname" required/>
                        </div>
                        <div class="col-xs-12">
                            <input type="text" class="js-lastname" id="lastname" placeholder="Nazwisko"required
                                name="lastname" />
                        </div>
                        <div class="col-xs-12">
                            <input type="email" class="js-email" id="email" placeholder="Podaj swój e-mail" required
                                name="email" />
                        </div>
                        <div class="col-xs-12" style="margin-bottom:1rem">
                            <div style="height: 40px;top: 1px;margin-left: 1px;background-image: url('f_img/pl.png');background-size: 55%;background-repeat: no-repeat;background-position: center;text-align:right;position: absolute;width: 5rem;pointer-events:none;background-color: #e0e0e0;"></div>
                            <input type="text" value="<?=$number?>" name="phone_code" readonly style="background:none;text-align:right;position: absolute;width: 10rem;pointer-events:none;border-right: none;opacity: 0.7;">
                            <input type="tel" style="padding-left:11rem" class="js-phone" id="phone" name="phone_number" minlength="7" placeholder="Numer Telefonu" required/>
                        </div>
                        <div class="col-xs-12 offer_row">
                            <input type="checkbox" name="oferta" checked="" class="js-oferta" />
                            <span style="color: #fff;">Wyrażam zgodę na przetwarzanie danych osobowych i otrzymywanie
                                materiałów reklamowych oraz zgadzam się z <a href="oferta.php" style="color: #fff;"
                                    class="f_lnk" target="_blank">ofertą publiczną</a></span>
                        </div>
                        <div class="col-xs-12">
                            <input type="submit" class="subscribeBtn" name="submitBtn"
                                value="Uzyskaj natychmiastowy dostęp">
                        </div>
                    </form>

                    <div class="images">
                        <img src="pl_site_files/payment.png" alt="">
                        <img src="pl_site_files/verified.png" alt="">
                        <img src="pl_site_files/safe.png" alt="">
                    </div>

                </div>
            </div>

        </div>
    </div>

    <script src="pl_site_files/jquery-1.js"></script>
    <script src="pl_site_files/bootstrap.js"></script>
    <script src="f_js/script.js"></script>

</body>
</html>